# Automatisierte Demo

This project is to test automation Demos for the WebGui

# Git Functionalities
## Automated Git pull

Git_Auto.py and auto_pull.bat can be used to pull git repository automatically at a scheduled time

## Requirements
- Git
- Python 3
- Gitpython

## Setup
- Create an account in Confluence (For the future Documentations and tutorials as well)
- Setup Git following this tutorial: [Git](https://srv01cf1.corp.trumpf.com/pages/viewpage.action?pageId=344556386)
- Setup python: [py setup](https://srv01cf1.corp.trumpf.com/pages/viewpage.action?pageId=671812161)
- Make sure python and pip are in Environment variables if not add them
- Setup proxy for pip [pip proxy](https://srv01cf1.corp.trumpf.com/pages/viewpage.action?pageId=590285124)
- Install Gitpython using:  "pip install GitPython"
- Clone the repository you need to auto pull to the local machine
- Edit the path to repository on local machine in Git_Auto.py
- Edit the path to python and Git_Auto.py in auto_pull.bat
- follow this tutorial to setup the script to run at scheduled time: [Schedular](https://datatofish.com/python-script-windows-scheduler/)
